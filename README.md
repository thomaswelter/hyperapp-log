# Log for Hyperapp
This plugin adds a timetravel debugger to [hyperapp][1]
try it out **[here][2]**

[1]: https://github.com/jorgebucaran/hyperapp
[2]: https://thomaswelter.gitlab.io/hyperapp-log

![debugger example](usage-clip-small.gif)

There are two files:
- `hyperapp-log.js` adds a log element to the page (open with `ctrl+\``)
- `hyperapp-logAction.js` log actions to the console

## hyperapp-log
```javascript
import {h, app} from './hyperapp.js'
import log from './hyperapp-log.js'

// declare state, actions and view

log(h, app)(state, actions, view, document.body)
// instead of app(state, actions, view, document.body)
```

## hyperapp-logAction
![log-action](log-action-console.jpg)
```javascript
import {h, app} from './hyperapp.js'
import logAction from './hyperapp-logAction.js'

// declare state, actions and view

logAction(app)(state, actions, view, document.body)
// instead of app(state, actions, view, document.body)
```