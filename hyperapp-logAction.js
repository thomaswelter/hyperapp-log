export default app => (state, actions, view, element) => {
	let globalState = state

	const isFunction = val =>
		typeof val == 'function'

	const log = (name, path, value, partial) => {
		console.groupCollapsed(`${path.concat(name).join('.')}( ${value} )`)
			console.log(JSON.stringify(partial, null, '\t'), partial)

			console.groupCollapsed('...')
				console.log(JSON.stringify(globalState, null, '\t'))
			console.groupEnd()
		console.groupEnd()
	}

	const proxyAction = (action, name, path) => value => (state, actions) => {
		let result = action(value)
		result = (isFunction(result)? result(state, actions) : result)
		let partialState = {...state, ...result}
	
		if(result && globalState !== result) {		
			if(!path.length)
				globalState = partialState

			else {
				let s = globalState
				while(path.length -1)
					s[path.shift()]
				s[path[0]] = partialState
			}

			log(name, path, value, result)
		}

		return result
	}

	const enhancheActions = (actions, path = []) => {
		for(let k in actions)
			isFunction(actions[k])
				? actions[k] = proxyAction(actions[k], k, path)
				: enhancheActions(actions[k], path.concat(k))
	}

	enhancheActions(actions)

	return app(state, actions, view, element)
}