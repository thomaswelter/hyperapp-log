const isPrimitive = val =>
	['boolean', 'number', 'string', 'undefined'].indexOf(typeof val) > -1 || val === null

const diff = (a, b, path = [], detail = true) => {
	if(a === b)
		return []

	if(!isPrimitive(b)) {
		// return [...Object.keys(a), ...Object.keys(b)]
		return Object.keys(b).concat(isPrimitive(a)? []: Object.keys(a))
			// filter double common keys
			.filter((n, i, r) => r.indexOf(n) == i)

			// recursive diff
			// detail: also log changed objects {} -> {}
			.reduce((acc, k) => acc.concat(diff((isPrimitive(a)? undefined : a[k]), b[k], path.concat(k)))
				, [(detail? [a, b, path] : [])])

			// filter empty logs
			.filter(x => x.length)

			// filter removal of keys
			.filter(x => x[1] !== undefined)
	}

	return [[a, b, path]]
}

const toElements = (h, diff = []) => {
	const base = {
		// padding: '5px 0px'
		// whiteSpace: 'pre'
	}

	const run = (o, path = []) => {
		const hasChanged = !!diff.find(([a, b, p]) => p.join('.') == path.join('.'))
		const changeStr = hasChanged? 'rgba(0, 0, 0, .1)' : 'none'

		if(typeof o == 'string') {
			return [h('span', {style:{
				...base,
				color: 'DarkGoldenRod',
				background: changeStr
			}}, `'${o}'`)]
		}

		else if(typeof o == 'number') {
			return [h('span', {style: {
				...base,
				color: 'purple',
				background: changeStr
			}}, o + '')]
		}

		else if(typeof o == 'boolean') {
			return [h('span', {style: {
				...base,
				color: 'purple',
				background: changeStr
			}}, o + '')]
		}

		else if(Array.isArray(o)) {
			return [
				h('span', {style: {...base, color: 'grey', background: changeStr}}, '[', h('br')),
				...o.map((n, i) => h('div', {style: {...base, marginLeft: ((path.length +1) * 10) + 'px'}},
					run(n, path.concat(i + '')),
					h('span', {style: {...base, color: 'grey'}}, ','),
					h('br')
				)),
				h('span', {style: {...base, color: 'grey', background: changeStr}}, ']')
			]
		}

		else {
			return [
				h('span', {style: {...base, color: 'grey', background: changeStr}}, '{', h('br')),
				...Object.entries(o).map(([k, v]) => {
					const changedKey = !!diff.find(([a, b, p]) => p.join('.') == path.concat(k).join('.'))

					return h('div', {style: {...base, marginLeft: ((path.length +1) * 10) + 'px'}},
						h('span', {style: {...base, color: 'grey', background: changedKey? 'rgba(0, 0, 0, .1)' : 'none'}}, k),
						h('span', {style: {...base, color: 'grey'}}, ': '),
						run(v, path.concat(k)),
						h('span', {style: {...base, color: 'grey'}}, ','),
						h('br')
					)
				}),
				h('span', {style: {...base, color: 'grey', background: changeStr}}, '}')
			]
		}
	}

	return run
}

const mergePartialState = (state, partial, path) => {
	if(!path.length)
		return {...state, ...partial}

	let s = state
	while(path.length > 1)
		s = state[path.shift()]

	s[path.shift()] = partial
	return {...state}
}

const logApp = (h, app) => {
	const state = {
		actionList: [],
		actionListFiltered: [],
		stateElements: undefined,
		initialState: {},
		hidden: false,
		touchStartPos: null,
		scrollProgress: 1,
		scrollDirty: true,
		syncUI: true,
		filterDuplicates: false
	}

	const actions = {
		step: t => (state, actions) => {
			requestAnimationFrame(actions.step)

			actions.stepUpdate()
		},

		initialState: data => state => {
			return { initialState: data, stateElements: toElements(h)(data) }
		},

		action: ({name, path, value, partial}) => state => {
			const lastAction = state.actionList[state.actionList.length -1]
			const lastState = lastAction && lastAction.fullState || state.initialState
			const fullState = mergePartialState(lastState, partial, path)

			const d = diff(lastState, fullState)

			return {
				actionList: state.actionList.concat({
					name: path.concat(name).join(''),
					path, value, partial, fullState,
					selected: false,
					key: key++
				}),

				stateElements: toElements(h, d)(fullState),

				scrollDirty: true,
				scrollProgress: 1
			}
		},

		scroll: ([delta, total]) => ({ scrollProgress: Math.min(delta / total, 1), scrollDirty: true }),

		stepUpdate: (force = false) => state => {
			if(!state.scrollDirty && !force)
				return

			const i = Math.floor(state.actionList.length * state.scrollProgress)
			const prevSelectedIndex = state.actionList.slice().reverse().findIndex(n => n.selected)

			if(!prevSelectedIndex && i == 0)
				return ({ scrollDirty: false })

			if(state.actionList.length - prevSelectedIndex == i)
				return ({ scrollDirty: false })

			// const actionList = state.actionList.map((item, ii) => ({...item, selected: ii < i}))
			const actionList = state.actionList.map((item, ii) => {
				const selected = ii < i
				return item.selected == selected
					? item
					: {...item, selected}
			})
			const actionListFiltered = !state.filterDuplicates || force
				? actionList
				: actionList.reduce((l, item) => {
				const last = l.length && l[l.length -1]
				if(!last || last.name !== item.name)
					return l.concat(item)

				return l.slice(0, -1).concat(item)
			}, [])
			const prevSelected = state.actionListFiltered.slice().reverse().find(n => n.selected)
			const nextSelected = actionListFiltered.slice().reverse().find(n => n.selected)

			const stateElements = nextSelected
				? toElements(h, diff((prevSelected && prevSelected.fullState) || state.initialState, nextSelected.fullState))(nextSelected.fullState)
				: toElements(h)(state.initialState)

			if(state.syncUI || force)
				appActions.$$set_state(nextSelected? nextSelected.fullState : state.initialState)

			return { actionList, actionListFiltered, stateElements, scrollDirty: false }			
		},

		toggle: () => state => ({ hidden: !state.hidden }),

		saveTouchStart: ([x, y]) => ({ touchStartPos: [x, y] }),

		toggleOnSwipe: ([x, y]) => state => {
			if(!state.touchStartPos || !state.hidden || state.touchStartPos[0] < window.innerWidth - 10)
				return

			const dx = Math.abs(x - state.touchStartPos[0])
			const dy = Math.abs(y - state.touchStartPos[1])
			const swipeLeft = dx / dy > 1 && x < state.touchStartPos[0]
			return ({ touchStartPos: null, hidden: !swipeLeft })
		},

		toggleSyncUI: () => (state, actions) => {
			if(!state.syncUI)
				actions.stepUpdate(true)

			return ({ syncUI: !state.syncUI })
		},

		toggleFilterDuplicates: () => (state, actions) => {
			if(!state.filterDuplicates)
				actions.stepUpdate(true)

			return ({ filterDuplicates: !state.filterDuplicates })
		}
	}

	const view = (state, actions) => h('div', {style: {
		position: 'fixed',
		top: 0,
		right: state.hidden? '-100%' : 0,
		width: '500px',
		height: '100vh',
		transition: '.2s right ease',
		display: 'flex',
		flexDirection: 'column'
	}},

		// menu bar
		h('div', {style: {
			width: '100%',
			height: '50px',
			background: 'grey',
			display: 'flex',
			justifyContent: 'flex-end'
		}},

			h('button', {
				onclick: ev => actions.toggleSyncUI(),
				style: {
					padding: '10px',
					background: state.syncUI? 'rgba(0,0,0, .3)' : 'none',
					border: 0
				}
			}, 'Sync UI'),

			// TODO::
			// h('button', {
			// 	onclick: ev => actions.toggleFilterDuplicates(),
			// 	style: {
			// 		padding: '10px',
			// 		background: state.filterDuplicates? 'rgba(0,0,0, .3)' : 'none',
			// 		border: 0
			// 	}
			// }, 'Filter duplicates'),

			h('button', {
				onclick: ev => actions.toggle(),
				style: {
					padding: '10px',
					border: 0,
					background: 'none'
				}
			}, 'Close [Ctrl + `]')
		),

		h('div', {style: {
			width: '100%',
			flex: 1,
			display: 'flex'
		}},

			// state
			h('div', {style: {
				font: '12px sans-serif',
				padding: '10px',
				width: '60%',
				overflowY: 'auto',
				background: '#EEE'
			}}, state.stateElements),

			// list
			h('div', {
				onscroll: ev => {
					actions.scroll([ev.target.scrollTop, ev.target.scrollHeight - ev.target.offsetHeight])
				},
				onupdate: el => state.scrollProgress == 1 && el.scroll(0, 99999),
				style: {
					font: '12px sans-serif',
					overflowY: 'auto',
					overflowX: 'hidden',
					flex: 1,
					background: '#DDD',
					whiteSpace: 'nowrap',
					userSelect: 'none',
					height: '100vh'
				}
			},
				...state.actionListFiltered.map((item, i, r) => {
					const isFirst = i == 0
					const isLast = i == r.length -1

					return h('div', {
						key: item.key,
						style: {
							padding: '5px',
							background: item.selected? 'rgba(0, 0, 0, .1)' : 'none',
							font: '12px sans-serif',
							marginTop: isFirst? '50vh' : '0',
							marginBottom: isLast? '50vh' : '0',
						}
					},
						item.name,
						h('span', {style: {
							color: '#666',
							marginLeft: '5px'
						}}, JSON.stringify(item.value))
					)
				})
			)
		)
	)

	return app(state, actions, view, document.body)
	// return logAction(app)(state, actions, view, document.body)
}

const isFunction = val =>
	typeof val == 'function'

const proxyAction = (action, name, path) => value => (state, actions) => {
	let partial = action(value)
	partial = (isFunction(partial)? partial(state, actions) : partial)

	logAppActions.action({name, path, value, partial})

	return partial
}

const enhancheActions = (actions, path = []) => {
	for(let k in actions)
		isFunction(actions[k])
			? actions[k] = proxyAction(actions[k], k, path)
			: enhancheActions(actions[k], path.concat(k))
}

let logAppActions
let appActions
let key = 0

export default (h, app) => (state, actions, view, element) => {
	logAppActions = logApp(h, app)

	logAppActions.initialState(state)
	enhancheActions(actions)

	actions.$$set_state = data => state => {
		const empty = Object.keys(state).reduce((acc, k) => ({...acc, [k]: undefined}), {})
		return {...empty, ...data}
	}

	appActions = app(state, actions, view, element)

	// toggle on [ctrl + `]
	window.addEventListener('keydown', ev => ev.ctrlKey && ev.keyCode == 192 && logAppActions.toggle())

	// save touchstartpos for toggleOnSwipe
	window.addEventListener('touchstart', ev => {
		const t = ev.touches[0]
		logAppActions.saveTouchStart([t.pageX, t.pageY])
	})

	// toggle on swipe in from right (on touchscreen)
	window.addEventListener('touchmove', ev => {
		const t = ev.touches[0]
		logAppActions.toggleOnSwipe([t.pageX, t.pageY])
	})

	// run raf
	logAppActions.step(0)

	return appActions
}