import {h, app} from './hyperapp.js'
import log from './hyperapp-log.js'

let key = 0

const state = {
	todos: [],
	input: ''
}

const actions = {
	updateInput: input => ({ input }),

	addTodo: () => state => ({ todos: state.todos.concat({name: state.input, done: false, key: key++}), input: '' }),

	toggleDone: key => state => ({
		todos: state.todos.map(todo => todo.key == key? {...todo, done: !todo.done} : todo)
	})
}

const view = (state, actions) => h('div', {},
	h('h1', {}, 'TODO'),

	...state.todos.map(todo => {
		return h('div', {
			key: todo.key,
			onclick: () => actions.toggleDone(todo.key),
			style: {
				cursor: 'pointer',
				userSelect: 'none'
			}
		}, todo.name, ` [${todo.done? 'x' : ''}]`)
	}),

	h('input', {
		placeholder: 'new todo item',
		autofocus: true,
		value: state.input,
		oninput: ev => actions.updateInput(ev.target.value),
		onkeyup: ev => ev.key == 'Enter' && actions.addTodo()
	})
)

log(h, app)(state, actions, view, document.body)